<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->integer('country')->nullable();
            $table->tinyInteger('gender')->nullable();
            $table->tinyInteger('relationship_status')->nullable()->default(null);  //single married
            $table->string('dob')->nullable();
            $table->tinyInteger('education')->nullable();
            $table->tinyInteger('body_type')->nullable(); //slim smart
            $table->tinyInteger('have_children')->nullable();
            $table->tinyInteger('zodiac_sign')->nullable();
            $table->tinyInteger('sexual_orientation')->nullable();
            $table->tinyInteger('smoker')->nullable();
            $table->tinyInteger('drink')->nullable();
            $table->tinyInteger('sports')->nullable();
            $table->tinyInteger('height')->nullable();
            $table->tinyInteger('hair_color')->nullable();
            $table->tinyInteger('eye_color')->nullable();
            $table->string('ethnicity')->nullable();
            $table->string('tag')->nullable()->default(null);
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('is_social')->default(0);
            $table->string('photo')->nullable();
            $table->string('verify_token')->nullable();
            $table->string("city")->nullable()->default(null);
            $table->string("state_name")->nullable()->default(null);
            $table->string("postal_code")->nullable()->default(null);
            $table->string("lat")->nullable()->default(null);
            $table->string("lon")->nullable()->default(null);
            $table->string("currency")->nullable()->default(null);
            $table->tinyInteger("i_am")->nullable()->default(null);
            $table->tinyInteger("lk_for")->nullable()->default(null);
            $table->tinyInteger("min_age")->nullable()->default(null);
            $table->tinyInteger("max_age")->nullable()->default(null);
            $table->tinyInteger("s_photos")->nullable()->default(null);
            $table->tinyInteger("s_online")->nullable()->default(null);
            $table->tinyInteger("s_r_status")->nullable()->default(null);  //relationship status
            $table->integer("s_country")->nullable()->default(null);  //country
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->string("device_token")->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
