<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable()->default(null);
            $table->string('currency')->nullable()->default(null);
            $table->bigInteger('price')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->string('duration')->nullable()->default(null);
            $table->string('duration_type')->nullable()->default(null);
            $table->bigInteger('coins')->nullable()->default(null);
            $table->tinyInteger('status')->nullable()->default(0);
            $table->string('photo')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
