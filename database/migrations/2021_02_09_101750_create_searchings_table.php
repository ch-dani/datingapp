<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSearchingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('searchings', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->unique();
            $table->tinyInteger('education')->nullable()->default(null);
            $table->tinyInteger('body_type')->nullable()->default(null); //slim smart
            $table->tinyInteger('have_children')->nullable()->default(null);
            $table->tinyInteger('zodiac_sign')->nullable()->default(null);
            $table->tinyInteger('sexual_orientation')->nullable()->default(null);
            $table->tinyInteger('smoker')->nullable()->default(null);
            $table->tinyInteger('drink')->nullable()->default(null);
            $table->tinyInteger('sports')->nullable()->default(null);
            $table->tinyInteger('height')->nullable()->default(null);
            $table->tinyInteger('hair_color')->nullable()->default(null);
            $table->tinyInteger('eye_color')->nullable()->default(null);
            $table->tinyInteger('ethnicity')->nullable()->default(null);
            $table->tinyInteger('gender')->nullable()->default(null);
            $table->tinyInteger('travel')->nullable()->default(null);
            $table->tinyInteger('private_photo')->nullable()->default(null);
            $table->tinyInteger('responsiveness')->nullable()->default(null);
            $table->tinyInteger('attractiveness')->nullable()->default(null);
            $table->tinyInteger('status')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('searchings');
    }
}
