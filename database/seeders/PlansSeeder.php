<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Plan;

class PlansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Plan::create([
        	"name"=>"premium",
        	"currency"=>"EAD",
        	"price"=>4500,
        	"description"=>"greate Pkd",
        	"duration"=>"6",
        	"duration_type"=>"month",
        	"coins"=>10000,
        	"status"=>1,
        ]);
         Plan::create([
        	"name"=>"Silver",
        	"currency"=>"PAK",
        	"price"=>4000,
        	"description"=>"greate Pkd",
        	"duration"=>"4",
        	"duration_type"=>"week",
        	"coins"=>1000,
        	"status"=>1,
        ]);

    }
}
