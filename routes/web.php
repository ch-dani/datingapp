<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AdminControllers\AdminController;
use App\Http\Controllers\AdminControllers\UserController;
use App\Http\Controllers\AdminControllers\PlansController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix'=>'dani'],function(){
	Route::get('dash',[AdminController::class,'index']);
});

Route::group(['prefix'=>'admin/user'],function(){
	Route::get('show-all',[UserController::class,'index'])->name('users.index');

	
	Route::get('{id}/followings',[UserController::class,'user_followings'])->name('user.followings');
	
	Route::get('{id}/likings',[UserController::class,'user_likings'])->name('user.likings');
	
	Route::get('delete/{id}',[UserController::class,'destroy'])->name('user.del');
});

Route::group(['prefix'=>'admin/plans'],function(){
	Route::get('show-all',[PlansController::class,'index'])->name('plans.index');
	Route::get('delete/{id}',[PlansController::class,'destroy'])->name('plan.del');
});



// run commands

Route::get('/run-migrations', function () {
	return Artisan::call('migrate');
});
Route::get('/storage-link',function(){
	return Artisan::call('storage:link');
});