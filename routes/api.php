<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiControllers\AuthController;
use App\Http\Controllers\ApiControllers\FollowsController;
use App\Http\Controllers\ApiControllers\LikesController;
use App\Http\Controllers\ApiControllers\SubscriptionsController;
use App\Http\Controllers\ApiControllers\SocialController;
use App\Http\Controllers\ApiControllers\SearchController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware'=>'apiRecord'],function(){
	Route::group(['middleware'=>'auth:api'],function(){
		Route::post('logout',[AuthController::class,'logout']);
		Route::post('save-user-info',[AuthController::class,'save_user_info']);
		Route::post('update-user-info',[AuthController::class,'update_user_info']);
		Route::post('update-profile-image',[AuthController::class,'update_profile_pic']);
		Route::post('get-user-info',[AuthController::class,'get_user_info']);
		Route::post('save-user-image',[AuthController::class,'save_user_image']);
		
		Route::post('search-nearest',[AuthController::class,'search_nearest']);
		Route::post('to-follow',[FollowsController::class,'to_follow']);
		Route::post('to-like',[LikesController::class,'to_like']);
		Route::get('get-all-plans',[SubscriptionsController::class,'get_all_plans']);

		Route::post('basic-search',[AuthController::class,'basic_search']);
		Route::get('basic-search-user',[AuthController::class,'basic_search_user']);
		Route::post('user-profile',[AuthController::class,'user_profile']);
		Route::get('get-online-users',[AuthController::class,'get_online_users']);


		Route::get('user/profile',[AuthController::class,'get_user_profile']);
		Route::post('is-phone-verified',[AuthController::class,'is_phone_verified']);
		Route::post('verify-phone',[AuthController::class,'phone_verified']);

	});

	Route::get('search-match',[AuthController::class,'search_match']);

	Route::post('login',[AuthController::class,'login'])->name('login');

	Route::post('register',[AuthController::class,'register']);
	Route::post('register-social',[AuthController::class,'social_register']);
	Route::post('get-verification-code',[AuthController::class,'get_code_gmail']);
	Route::post('confirm-verification-code',[AuthController::class,'confirm_code']);
	// test api

	// open apis
	
	Route::get('get-all-users',[AuthController::class,'get_all_users']);

	// Route::post('upload-image',[AuthController::class,'up_img']);



	



	// social portions api

	Route::group(['middleware'=>'auth:api'],function(){
		Route::post('like/image',[SocialController::class,'like_image']);
		Route::post('like/video',[SocialController::class,'like_video']);
		Route::post('upload/image',[SocialController::class,'upload_image']);
		Route::post('upload/video',[SocialController::class,'upload_video']);
		Route::post('follow/user',[SocialController::class,'follow_user']);
		// get user gallery
		Route::get('user/gallery',[SocialController::class,'get_user_all_gallery']);
		Route::post('user/private-gallery',[SocialController::class,'get_user_private_gallery']);
		Route::get('user/public-gallery',[SocialController::class,'get_user_public_gallery']);
		Route::get('user/video-gallery',[SocialController::class,'get_user_video_gallery']);
		Route::get('user/notification-list',[SocialController::class,'notifications_list']);
		Route::get('user/like-notifications',[SocialController::class,'like_notifications']);
		Route::get('user/follow-notifications',[SocialController::class,'follow_notifications']);
		Route::post('user/notification-read',[SocialController::class,'notifications_read']);
	});

	// advance searching
	Route::group(['middleware'=>'auth:api'],function(){
		Route::post('save-advance-search-parameter',[SearchController::class,'save_parameter']);
		Route::post('update-advance-search-parameter',[SearchController::class,'update_search_parameter']);
		Route::get('get-search-parameters-with-user',[SearchController::class,'get_searching_parameter']);
		Route::get('get-advance-search-users',[SearchController::class,'get_user_with_advance_search']);
	});


	
	// Route::get('test-notifications',[SocialController::class,'testNotification']);
});