@extends('admin.layouts.master')
@section('title','show all plans')
@section('main_content')
	<div>

		<table class="table table-hover data-table user_table">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Price</th>
					<th>Coins</th>
                    <th>Duration</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
		</table>
	</div>
@endsection
@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">

	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
	

	<script type="text/javascript">
		$("document").ready(function(){

		})
		
		$(".user_table").DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('plans.index') }}",
        columns: [
            {
            	data: 'id', 
            	name: 'id'
            },
            
            {
            	data: 'name', 
            	name: 'name'
            },
            {
            	data: 'price', 
            	name: 'price'
            },
            {
            	data: 'coins', 
            	name: 'coins'
            },
            {
            	data: 'duration', 
            	name: 'duration'
            },
            {
            	data: 'status',
            	name: 'status'
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            
            
        ]
    });
		
	</script>
@endsection