<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;


class Video extends Model
{
	use HasFactory;
	protected $fillable=[
		"user_id",
		"video",
		"status",
	];
	// protected $appends=array("likes");
	public function getLikesAttribute(){
		// return $this->video;
		$cnt=Like::where('video_id',$this->id)->count();
		// return $cnt;
	}
	public function getVideoAttribute(){
        return ($this->attributes['video'] != null) ? Storage::disk('s3')->url('galleryImages/'.$this->attributes['video']) : null;
    }

    
    public function user(){
    	return $this->belongsTo(User::class);
    }
}
