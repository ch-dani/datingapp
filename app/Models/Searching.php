<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Searching extends Model
{
	protected $fillable=[
		"education",
	    "body_type",
	    "have_children",
	    "zodiac_sign",
	    "sexual_orientation",
	    "smoker",
	    "drink",
	    "sports",
	    "user_id"
	];
    use HasFactory;
}
