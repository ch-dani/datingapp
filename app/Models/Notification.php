<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Notification extends Model
{
    use HasFactory;
    protected $fillable=[
    	"user_id",
    	"who_send_notification",
    	"body",
    	"action",
    	"photo",
    	"status"
    ];

    // public function getPhotoAttribute(){
    //     return ($this->attributes['photo'] != null) ? Storage::disk('s3')->url('images/'.$this->attributes['photo']) : null;
    // }

    public function getBodyAttribute(){
        return ($this->attributes['body'] != null) ? json_decode($this->attributes['body']) : null;
    }

}
