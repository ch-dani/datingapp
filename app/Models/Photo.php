<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Photo extends Model
{
    use HasFactory;
    protected $fillable=[
    	"user_id",
    	"image",
    	"status"
    ];
    protected $table='images';
    public function getImageAttribute(){
        return ($this->attributes['image'] != null) ? Storage::disk('s3')->url('galleryImages/'.$this->attributes['image']) : null;
    }

    public function user(){
    	return $this->belongsTo(User::class);
    }
}
