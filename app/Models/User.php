<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Storage;
use Cache;
use App\Models\Photo;
use App\Models\Follow;
use App\Models\Video;
use Carbon\Carbon;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'country',
        'gender',
        'relationship_status',
        'dob',
        'education',
        'body_type',
        'have_children',
        'zodiac_sign',
        'sexual_orientation',
        'smoker',
        'drink',
        'sports',
        'height',
        'hair_color',
        'eye_color',
        'ethnicity',
        'tag',
        'status',
        'is_social',
        'photo',
        'password',
        'verify_token',
        'city',
        'postal_code',
        'currency',
        'lat',
        'lon',
        'state_name',
        'i_am',
        'lk_for',
        'min_age',
        'max_age',
        's_photos',
        's_online',
        's_r_status',
        's_country',
        'device_token'
    ];
    protected $hidden = [
        'password',
        'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $appends = array('age','isOnline','rimg','userinfo');

    public function getRimgAttribute(){

        $image=Photo::where('user_id',$this->attributes['id'])->where('status',1)->orderBy('id', 'desc')->take(1)->first(['id','image']);
        return $image;
    }
    public function getPhotoAttribute(){
        return ($this->attributes['photo'] != null) ? Storage::disk('s3')->url('galleryImages/'.$this->attributes['photo']) : null;
    }

    public function getAgeAttribute(){
        return ($this->attributes['dob'] != null) ? Carbon::parse($this->attributes['dob'])->age : null;
    }

    public function getIsOnlineAttribute(){
        return (Cache::get('user-is-online-'.$this->id)!=null)?true:false;
    }
     public function getCityAttribute(){
         return ($this->attributes['city'] != null) ? $this->attributes['city']  : 'lahore';
    }
    public function getUserinfoAttribute(){
        $user=$this->attributes;
        $meta['followers']=Follow::where('following_id',$user['id'])->count();
        $meta['images']=Photo::where('user_id',$user['id'])->count();
        $meta['videos']=Video::where('user_id',$user['id'])->count();
        $meta['completed']=$this->profile_completion_percentage($user);
        return $meta;
    }

    // public function isOnline(){
    //     return Cache::get('user-is-online-' . $this->id);
    // }




    public function profile_completion_percentage($user){
        if(gettype($user)!='array'){
            $user= $user->toArray();    
        }

        unset($user["isOnline"]);
        unset($user["created_at"]);
        unset($user["updated_at"]);
        unset($user["email_verified_at"]);
        unset($user["lat"]);
        unset($user["lon"]);
        unset($user["postal_code"]);
        unset($user["state_name"]);
        unset($user["verify_token"]);
        unset($user["is_social"]);
        unset($user["status"]);
        unset($user["id"]);
        unset($user["hair_color"]);
        unset($user["eye_color"]);
        unset($user["ethnicity"]);
        unset($user["city"]);
        unset($user["currency"]);
        unset($user["i_am"]);
        unset($user["lk_for"]);
        unset($user["min_age"]);
        unset($user["max_age"]);
        unset($user["s_photos"]);
        unset($user["s_r_status"]);
        unset($user["s_online"]);
        unset($user["s_country"]);
        unset($user["device_token"]);
        unset($user["rimg"]);
        $percentage=0;
        $per_field=100/count($user);
        foreach ($user as $key => $value){
            if($value!=null){
                $percentage +=$per_field;
            }
        }
        if((int)$percentage>=99){
            return 100;
        }
        return (int)$percentage;
    }

  


   
}
