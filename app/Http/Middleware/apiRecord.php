<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Cache;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;

class apiRecord
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        
        if($user=auth()->guard('api')->user()){
            Cache::put('user-is-online-'.$user->id,true,1800);    
            // Cache::flush();
            // return response()->json(Cache::get('user-is-online-1'));
        }
        $log=[
            'IP'=>$request->ip(),
            // 'USER'=>$request->user()->id,
            'URL'=> $request->url(),
            'REQUEST_BODY'=> $request->all(),
            // 'RESPONSE'=> $response->getContent()
        ];
        $log=json_encode($log);
        Log::channel('apiChannel')->info($log);
        
        return $next($request);
    }
}
