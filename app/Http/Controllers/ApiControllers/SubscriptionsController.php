<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Plan;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;

class SubscriptionsController extends Controller
{
   
    public function index()
    {
        //
    }
    public function get_all_plans(){
        $user=auth()->guard('api')->user();
        if($user->verify_token=='verified'){
            $verify=true;
        }else{
            $verify=false;
        }
        $allPlans=Plan::where('status',1)->get(['id','name','price','coins','duration','duration_type']);
        return response()->json(['status'=>1,'verify'=>$verify,'plans'=>$allPlans]);
        // return 'received plans';
    }

    
   
}
