<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use DB;
use App\Models\User;
use App\Models\Follow;
use App\Models\Photo;
use App\Models\Video;
use App\Models\Searching;
use Illuminate\Database\Eloquent\Builder;
use App\Mail\forgot_pwd;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use GeoIP;
use Image;
use Cache;


class AuthController extends Controller
{
	// $user_attributes_array=['name','email','phone','country','gender','relationship_status','dob','education','body_type','have_children','zodiac_sign','sexual_orientation','smoker','drink','sports','height','hair_color','eye_color','ethnicity','status','is_social','photo','verify_token','city','postal_code',
 //        'currency','lon','state_name','i_am','lk_for','min_age','max_age','s_photos','s_online','s_r_status','s_country',];
    //

    protected $attrToUser=['id','name','email','phone','country','city','gender','relationship_status','dob','education','body_type','have_children','zodiac_sign','sexual_orientation','tag','smoker','drink','sports','height','hair_color','eye_color','ethnicity','is_social','photo'];


    public function token_key(){
    	return 'testToken';
    }
    
   

    public function register(Request $request){

    	$validator = Validator::make($request->all(), [
	      'email' => 'required|unique:users',
	      'password' => 'required',
	      'device_token'=>'required',
	    ]);

	    if ($validator->fails()) {
	      return response()->json(['status' => 0, 'message'=>$this->get_errors($validator->messages())]);
	    }
	    try{
	    	DB::beginTransaction();
	    	$user=User::create([
	    		"email"=>$request->email,
	    		"password"=>Hash::make($request->password),
	    		"device_token"=>$request->device_token
			]);
			Cache::put('user-is-online-'.$user->id,true,1800);
			$token=$user->createToken($this->token_key())->accessToken;
	    	DB::commit();
	    	return response()->json(['status'=> 1, 'token'=>$token]);
	    }
	    catch(\Exception $e){
	    	DB::rollback();
	    	if(env("APP_ENV"=="local")){
	    		dd($e);
	    	}else{
	    		return response()->json(['status'=>0,'message'=>'database error']);
	    	}
	    
	    }
	   
    }
    public function is_phone_verified(Request $request){
    	$user=auth()->guard('api')->user();
    	if($user->verify_token=='verified'){
    		$verify=true;
    	}else{
    		$verify=false;
    	}
    	return response()->json(['status'=>1,'verify'=>$verify]);
    }
    public function social_register(Request $request){
	  $validator = Validator::make($request->all(), [
	      'email' => 'required|unique:users',
	      'is_social' => 'required',
	  ]);
	  if ($validator->fails()) {
	      return response()->json(['status' => 0, 'message'=>$this->get_errors($validator->messages())]);
	    }
	     try{
	    	DB::beginTransaction();
	    	$user=User::create([
	    		"email"=>$request->email,
	    		"is_social"=>$request->is_social
	    		
			]);
			$token=$user->createToken($this->token_key())->accessToken;
	    	DB::commit();
	    	return response()->json(['status'=> 1, 'token'=>$token]);
	    }
	    catch(\Exception $e){
	    	DB::rollback();
	    	if(env("APP_ENV"=="local")){
	    		dd($e);
	    	}else{
	    		return response()->json(['status'=>0,'message'=>'database error']);
	    	}
	    
	    }
	}
    public function save_user_info(Request $request){
    	// gencode
    	$user = auth::guard('api')->user();
    	
    	$validator = Validator::make($request->all(), [
	      "name"=>"required",
	      "phone"=>"required|min:10|max:13",
	      "country"=>"required",
	      "gender"=>"required",
	      "dob"=>"required",
	    ]);
	  	if ($validator->fails()){
	      return response()->json(['status' => 0, 'message'=>$this->get_errors($validator->messages())]);
	    }
		$ip= $request->ip();
    	// return geoip()->getLocation($ip)->state_name;
		
    	try{
    		DB::beginTransaction();
			$data = $request->all();
			$data['country']=$request->country;
			$data['postal_code']=geoip()->getLocation($ip)->postal_code;
			// $data['city']=geoip()->getLocation($ip)->city;
			// $data['state_name']=geoip()->getLocation($ip)->state_name;
			$data['lat']=geoip()->getLocation($ip)->lat;
			$data['lon']=geoip()->getLocation($ip)->lon;
			// $data['currency']=geoip()->getLocation($ip)->currency;
			if($request->password){
    			$data['password']=hash::make($request->password);
    		}
    		if($request->hasFile('photo')){
    			$photo=$request->file('photo');
    			$photoName=uniqid().'.'.$photo->getClientOriginalExtension();
    			$photo = Image::make($photo);
    			$photo->resize(800, 800, function ($constraint) {
				    $constraint->aspectRatio();
				});
				$resource = $photo->stream()->detach();
				Storage::disk('s3')->put('galleryImages/' . $photoName,$resource,'public');
				$data['photo']=$photoName;
				Photo::create([
	                "user_id"=>$user->id,
	                "image"=>$photoName,
	                "status"=>1,
            	]);
			}

    		$user->update($data);
    		
    		DB::commit();
    		return response()->json(['status'=>1,'message'=>'user information saved successfully']);
    	}
    	catch(\Exception $e){
    		if(env("APP_ENV")=="local"){
    			dd($e);
    		}else{
    			return response()->json(['status'=>0,'message'=>'database error']);
    		}
    	}
	}
	public function update_user_info(Request $request){
		$user = auth::guard('api')->user();
		$ip= $request->ip();
		try{
    		DB::beginTransaction();
			$data = $request->all();
			if($request->password){
    			$data['password']=hash::make($request->password);
    		}
    		if($request->hasFile('photo')){
    			$photo=$request->file('photo');
    			$photoName=uniqid().'.'.$photo->getClientOriginalExtension();
    			$photo = Image::make($photo);
    			$photo->resize(800, 800, function ($constraint) {
				    $constraint->aspectRatio();
				});
				$resource = $photo->stream()->detach();
				Storage::disk('s3')->put('galleryImages/' . $photoName,$resource,'public');
				$data['photo']=$photoName;
				Photo::create([
	                "user_id"=>$user->id,
	                "image"=>$photoName,
	                "status"=>1,
            	]);
			}
    		$user->update($data);
    		DB::commit();
    		$user=$user->only($this->attrToUser);
    		$x= $this->get_user_meta($user);
    		return response()->json([
    			'status'=>1,
    			'followers'=>$x['followers'],
    			'photos'=>$x['images'],
    			'videos'=>$x['videos'],
    			'profile_completion'=>$x['completed'],
				'message'=>'user information updated successfully',
				'user_details'=>$user
    		]);
    	}
    	catch(\Exception $e){
    		if(env("APP_ENV")=="local"){
    			dd($e);
    		}else{
    			return response()->json(['status'=>0,'message'=>'database error']);
    		}
    	}
	}

	public function update_profile_pic(Request $request){
		$user=auth()->guard('api')->user();
		if($request->has('photo')){
			$photo=Photo::find($request->photo)->getRawOriginal('image');
			$user->update([
				"photo"=>$photo
			]);
			return response()->json([
		 	'status' => 1,
		 	'message'=>'Profile image updated successfully',
		 	'photo'=>$user->photo
		 ]);

		}else{
			return response()->json([
		 	'status' => 0,
		 	'message'=>'Image Id is required to be profile image']);
		}
	}

	public function get_user_meta($user){
		if(gettype($user)!='array'){
			$user=$user->toArray();
		}
		$meta['followers']=Follow::where('following_id',$user['id'])->count();
		$meta['images']=Photo::where('user_id',$user['id'])->count();
		$meta['videos']=Video::where('user_id',$user['id'])->count();
		$meta['completed']=$this->profile_completion_percentage($user);
		return $meta;
	}

	public function get_online_users(Request $request){
		$user=auth()->guard('api')->user();
		$result=[];
		$users=User::where('status',1)->where('id','!=',$user->id)->paginate(5,$this->attrToUser);
		
		foreach ($users as $user) {
			if($user->isOnline){
				array_push($result, $user);
				
			}
			
		}
		

		$users=$users->toArray();
		// $result['next_page_url']=$users['next_page_url'];
		 return response()->json([
		 	'status' => 1,
		 	'next_page_url'=>$users['next_page_url'],
		 	'users'=>$result
		 ]);
	
	}
	public function get_user_info(Request $request){
		$user=auth::guard('api')->user();
		$followers=Follow::where('following_id',$user->id)->count();
		$images=Photo::where('user_id',$user->id)->count();
		$videos=Video::where('user_id',$user->id)->count();
		$user=$user->only($this->attrToUser);
		return response()->json([
			'status'=>1,
			'profile_completion'=>$this->profile_completion_percentage($user),
			'followers'=>$followers,
			'videos'=>$videos,
			'photos'=>$images,
			'user_details'=>$user
		]);
	}

	public function get_code_gmail(Request $request){
		$validator = Validator::make($request->all(), [
	      'email' => 'required'
	    ]);
	    if($validator->fails()) {
	      return response()->json(['status' => 0, 'message'=>$this->get_errors($validator->messages())]);
	    }
	    $code= rand(100000,999999);
	    $details=[
	    	'title' => 'Verification Code',
	    	'body' => 'Your six digit verification code is '.$code
	    ];
	    $details='Your six digit verification code is '.$code;
	    try{
	    	DB::beginTransaction();
	    	$user=User::where('email',$request->email)->first();
	    	$user->update(["verify_token"=>$code]);
	    	Mail::to($request->email)->send(new forgot_pwd($code));
			if(Mail::failures()) {
	            return response()->json(['status'=> 0,'message'=>'Email cannot be sent! try later']);
	        }
	        DB::commit();
	        return response()->json(['status'=> 1,'message'=>'A mail has been sent to given mail.Please verify code']);
	    }
	    catch(\Exception $e){
	    	DB::rollback();
	    	if(env("APP_ENV")=="local"){
	    		return $e;
	    	}else{
	    		return response()->json(['status'=> 0,'message'=>'database error in email sending']); 
	    	}
	    }
	}
	public function confirm_code(Request $request){
		$user=User::where('email',$request->email)->where('verify_token',$request->token)->first();
		if($user){
			try{
				DB::beginTransaction();
				$user->update(['verify_token'=>1]);
				$user->tokens->each(function($token, $key) {
			        $token->delete();
			    });
				DB::commit();
				$token=$user->createToken($this->token_key())->accessToken;
				return response()->json(['status'=>1,'message'=>'token is verified! reset password','token'=>$token]);
			}catch(\Exception $e){
				DB::rollback();
				if(env("APP_ENV")=="local"){
					dd($e);
				}else{
					return response()->json(['status'=>0,'message'=>'database error']);
				}
			}
		}else{
			return response()->json(['status'=>0,'message'=>'wrong verification code']);
		}
	}

	protected $allSearchedUser;
	public function search_match(Request $request){
		$allusers=User::where('status',1)->paginate(2);
		return $allusers;
		$allattributes=$request->except(['page']);
		$i=0;
		foreach ($allattributes as $key => $value){

			if($key=='minAge' || $key=='maxAge'){
				continue;
			}
			$user=User::where($key,$value)->get($this->attrToUser);
			if($i==0){
				$this->allSearchedUser=$user;
				++$i;
			}else{
				$this->allSearchedUser=$this->allSearchedUser->merge($user);
			}
		}
		if($request->has(['minAge','maxAge'])){
			$minAge=Carbon::today()->subYears(10);
			$maxAge=Carbon::today()->subYears(12);
			$user=User::whereBetween('dob',[$maxAge,$minAge])->get();
			if($this->allSearchedUser){
				$this->allSearchedUser=$this->allSearchedUser->merge($user);
			}
			else{
				$this->allSearchedUser=$user;	
			}
			
		}

		return response()->json(['status'=>1,'users'=>$this->allSearchedUser]);


		// if($request->has('page')){
		// 	$page=$request->page;
		// 	if($page<=0 || $page==null)
		// 		$page=1;
		// }else{
		// 	$page=1;
		// }

		// $total_records=count($this->allSearchedUser);
		
		// if($total_records%10>0){
		// 	$total_pages= (int)($total_records/10)+1;
		// }else{
		// 	$total_pages= ($total_records/10);
		// }
		
		// $finalUsers=[];
		// $temp=($page-1)*10;
		// $till=$temp+10;
		// if($page<=$total_pages){
		// 	$count=0;
		// 	for($i=0;$i<$total_records;$i++) {
		// 		if($i>=$temp && $i<$till){
					
		// 		}else{
		// 			unset($this->allSearchedUser[$i]);
		// 		}
		// 	}
		// }
		// // return array_values(array($this->allSearchedUser));
		// return response()->json(['status'=>1,'total_users'=>$this->allSearchedUser->count(),'page'=>$page,'total_page'=>$total_pages,'users'=>$this->allSearchedUser]);
	}

	public function basic_search(Request $request){
		$validator = Validator::make($request->all(), [
	      'i_am' => 'required',
	      'min_age' => 'required',
	      'max_age' => 'required',
	      's_r_status' => 'required',
	      's_country' => 'required',
	    ]);
		if ($validator->fails()) {
			return response()->json(['status' => 0,'message'=>$this->get_errors($validator->messages())]);
	    }
		$user=auth()->guard('api')->user();
		$user->update($request->all());
		return response()->json(['status'=>1,'message'=>'search criteria has been saved']);
	}

	public function basic_search_user(Request $request){

		$user=auth()->guard('api')->user();
		// return $user;
		$minAge = $user->min_age;
		$maxAge = $user->max_age;
		$minDate = Carbon::today()->subYears($maxAge); // make sure to use Carbon\Carbon in the class
		$maxDate = Carbon::today()->subYears($minAge)->endOfDay();
		$attrs=$this->attrToUser;
		array_push($attrs,'recent_image');
		// return $attrs;
		$users=User::where('id', '!=',$user->id )
        ->where('gender',$user->lk_for)
        ->where('status',1)
        ->where('country',$user->s_country)
        ->Orwhere(function ($query) use ($user,$minDate,$maxDate) {
            $query->where('id', '!=',$user->id )
            ->where('gender',$user->lk_for)
            ->where('status',1)
            ->where('country',$user->s_country)
	        ->where('relationship_status',$user->s_r_status)
            ->whereBetween('dob',[$minDate,$maxDate]);
        })->paginate(5,$this->attrToUser);
		$users=$users->toArray();
		unset($users["links"]);
		unset($users["from"]);
		unset($users["last_page"]);
		unset($users["last_page_url"]);
		unset($users["per_page"]);
		unset($users["to"]);
		$users['status']=1;
		return $users;
	}

	public function user_profile(Request $request){
		$user=auth()->guard('api')->user();
		return response()->json(['status'=>true,'profile_completion'=>$this->profile_completion_percentage($user),'user_profile'=>$user]);
		// return $user;
	}

	public function search_nearest(Request $request){
		$ip=$request->ip();
		$allattributes= geoip()->getLocation($ip)->toArray();
		$data['country']=$allattributes['country'];
		$data['city']=$allattributes['city'];
		$data['state_name']=$allattributes['state_name'];
		$data['postal_code']=$allattributes['postal_code'];
		$data['lat']=$allattributes['lat'];
		$data['lon']=$allattributes['lon'];
		$i=0;
		foreach ($data as $key => $value){
			
			$user=User::where($key,$value)->get($this->attrToUser);
			if($i==0){
				$this->allSearchedUser=$user;
				++$i;
			}else{
				$this->allSearchedUser=$this->allSearchedUser->merge($user);
			}
		}
		return response()->json(['status'=>1,'total_users'=>$this->allSearchedUser->count(),'users'=>$this->allSearchedUser]);
	}
	public function get_user_profile(Request $request){
		$userA=auth()->guard('api')->user();
		$user=User::find($request->id);
		if(!$user){
			return response()->json([
				'status'=>0,
				'message'=>'The user with id you sent, does not exist'
			]);
		}
		$result=$this->get_user_meta($user);
		$result['matched_percent']=$this->matched_percentage($userA,$user);
		$gallery=Photo::where('user_id',$user->id)->get(['id','image','status','created_at']);
		// ['id','image','status','created_at'] pass to pageinage

		$gallery=$gallery->toArray();
		return response()->json([
			'status'=>1,
			'counters'=>$result,
			'gallery'=>$gallery
			
		]);
	}

	public function get_errors($errors){
		foreach ($errors->get('*') as $key => $value){
    		return $value[0];
    	}
	}

	public function profile_completion_percentage($user){
		if(gettype($user)!='array'){
			$user= $user->toArray();	
		}

		unset($user["isOnline"]);
		unset($user["created_at"]);
		unset($user["updated_at"]);
		unset($user["email_verified_at"]);
		unset($user["lat"]);
		unset($user["lon"]);
		unset($user["postal_code"]);
		unset($user["state_name"]);
		unset($user["verify_token"]);
		unset($user["is_social"]);
		unset($user["status"]);
		unset($user["id"]);
		unset($user["hair_color"]);
		unset($user["eye_color"]);
		unset($user["ethnicity"]);
		unset($user["city"]);
		unset($user["currency"]);
		unset($user["i_am"]);
		unset($user["lk_for"]);
		unset($user["min_age"]);
		unset($user["max_age"]);
		unset($user["s_photos"]);
		unset($user["s_r_status"]);
		unset($user["s_online"]);
		unset($user["s_country"]);
		unset($user["device_token"]);
		unset($user["rimg"]);
		$percentage=0;
		$per_field=100/count($user);
		foreach ($user as $key => $value){
			if($value!=null){
				$percentage +=$per_field;
			}
		}
		if((int)$percentage>=99){
			return 100;
		}
		return (int)$percentage;
	}

	
	

    public function login(Request $request){
    	$validator = Validator::make($request->all(), [
	      'email' => 'required',  //email may be email or phone number
	      'password' => 'required',
	    ]);
		if ($validator->fails()) {
			return response()->json(['status' => 0,'message'=>$this->get_errors($validator->messages())]);
	    }
	    if(is_numeric($request->email)){
			$credentials = [
		        'phone' => $request->email,
		        'password' => $request->password
		    ];
		}elseif(filter_var($request->email)){
		    $credentials = [
		        'email' => $request->email,
		        'password' => $request->password
		     ];
	    }


	    if(auth()->attempt($credentials)){
	    	$user=Auth::user();
	    	Cache::put('user-is-online-'.$user->id,true,1800);
	    	// return $user;	
	    	$token=$user->createToken($this->token_key())->accessToken;
	    	// $user= collect($user)->except('id','status','is_social','email_verified_at','verify_token','created_at','updated_at');
	    	$getElements=$this->attrToUser;
	    	array_push($getElements, 'isOnline');
	    	$user=$user->only($this->attrToUser);
	    	return response()->json(['status' => 1,'user_details'=>$user, 'token' => $token]);
		}else{
			return response()->json(['status' => 0, 'message'=>'Incorrect email or password']);
		}
	}

	public function logout(Request $request){
		$user=auth::guard('api')->user();
    	if($user->tokens){
    		foreach ($user->tokens as $token) {
    			$token->delete();
    		}
    	}
    	return response()->json(['status' => 1,'status_code'=>"logged out"]);
    }

    public function phone_verified(Request $request){
    	$user=auth()->guard('api')->user();
    	$user->update([
    		"verify_token"=>'verified'
    	]);
    	return response()->json([
			'status'=>1,
			'message'=>'verified successfully'
		]);
    }

    public function get_all_users(Request $request){
    	return User::get();

    	$user=auth()->guard('api')->user();
		$users=User::where('status',1)->where('id','!=',$user->id)->orderByDesc('id')->paginate(5,$this->attrToUser);
		$users=$users->toArray();
		return response()->json([
			"status"=>1,
			"current_page"=>$users["current_page"],
			"data"=>$users["data"],
			"first_page_url"=>$users["first_page_url"],
			"next_page_url"=>$users["next_page_url"],
			"prev_page_url"=>$users["prev_page_url"],
			"total"=>$users["total"],
		]);
		
	}



	public function matched_percentage($userA,$userB){
		if(gettype($userA)!='array'){
			$userA=$userA->toArray();
		}
		if(gettype($userB)!='array'){
			$userB=$userB->toArray();
		}
		$sparams=Searching::where('user_id',$userA['id'])->first();
		if(!$sparams){
			return 0;
		}
		$matched=0;
		$notMatched=0;
		$sparams=$sparams->only([
			"education",
		    "body_type",
		    "have_children",
		    "zodiac_sign",
		    "sexual_orientation",
		    "smoker",
		    "drink",
		    "sports",
		    "height",
		    "hair_color",
		    "eye_color",
		    "ethnicity",
		    "gender",
		    
		]);
		foreach ($sparams as $key => $value) {
			if($userB[$key]==$value){
				$matched += 8;
			}else{
				$notMatched += 8; 
			}
		}
		if($matched>100){
			return 100;
		}else{
			return $matched;
		}

		// return $notMatched.'and'.$matched;
	}
}
