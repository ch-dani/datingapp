<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Video;
use App\Models\Like;
use App\Models\Follow;
use App\Models\Photo;
use App\Models\Notification;
use Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use DB;
use Illuminate\Database\Eloquent\Builder;
use App\Mail\forgot_pwd;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use GeoIP;
use Image;

class SocialController extends Controller
{

    public function upload_image(Request $request){
        $validator = Validator::make($request->all(),[
          'photo' => 'required',
          'status'=>'required',
        ]);
        if($validator->fails()){
            return response()->json(['status' => 0,'message'=>$this->get_errors($validator->messages())]);
        }
        $user=auth()->guard('api')->user();

        try{
            DB::beginTransaction();
            $photo=$request->file('photo');
            $photoName=uniqid().'.'.$photo->getClientOriginalExtension();
            $photo = Image::make($photo);
            $photo->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            });
            $resource = $photo->stream()->detach();
            Storage::disk('s3')->put('galleryImages/' . $photoName,$resource,'public');
            Photo::create([
                "user_id"=>$user->id,
                "image"=>$photoName,
                "status"=>$request->status,
            ]);
            DB::commit();
            $meta=$this->get_user_meta($user);
            return response()->json([
                'status' =>1,
                'followers'=>$meta['followers'],
                'photos'=>$meta['images'],
                'videos'=>$meta['videos'],
                'profile_completion'=>$meta['completed'],
                'message'=>'Image uploaded Successfully!'
            ]);
        }
        catch(\Exception $e){
            DB::rollback();
            return response()->json(['status' =>0,'message'=>'Image cannot be uploaded!',"errors"=>$e]);
        }
    }
    public function upload_video(Request $request){
        $validator = Validator::make($request->all(),[
          'video' => 'required',
          'status'=>'required',
        ]);
        if($validator->fails()){
            return response()->json(['status' => 0,'message'=>$this->get_errors($validator->messages())]);
        }
        $user=auth()->guard('api')->user();
        try{
            DB::beginTransaction();

            $video=$request->file('video');

            $videoName=uniqid().'.'.$video->getClientOriginalExtension();

            // $video = Image::make($video);
            // $video->resize(500, 500, function ($constraint) {
            //     $constraint->aspectRatio();
            // });
            

            // $resource = $video->stream()->detach();
            $disk = Storage::disk('s3');
            $disk->put('videoGallery/'.$videoName, fopen($video, 'r+'));
            // return 'done';
            // Storage::disk('s3')->put('videoGallery/'.$videoName,'public');
            Video::create([
                "user_id"=>$user->id,
                "video"=>$videoName,
                "status"=>$request->status,
            ]);
            DB::commit();
            $meta=$this->get_user_meta($user);
            return response()->json([
                'status' =>1,
                'followers'=>$meta['followers'],
                'photos'=>$meta['images'],
                'videos'=>$meta['videos'],
                'profile_completion'=>$meta['completed'],
                'message'=>'video uploaded Successfully!'
            ]);
        }
        catch(\Exception $e){
            DB::rollback();
            return response()->json(['status' =>0,'message'=>'video cannot be uploaded!',"errors"=>$e]);
        }
        return 'pending uploading video';
    }
    public function like_image(Request $request){
        $validator = Validator::make($request->all(),[
          'photoid' => 'required|min:1',
        ]);
        if($validator->fails()){
            return response()->json(['status' => 0,'message'=>$this->get_errors($validator->messages())]);
        }
        $image=Photo::find($request->photoid);
        $user=auth()->guard('api')->user();
        $alreadyLiked=Like::where('user_id',$user->id)->where('image_id',$request->photoid)->count();
        // if($alreadyLiked){
        //     return response()->json(['status' => 0,'message'=>"You already liked ".$image->user->name."'s this image"]);
        // }
        
        try{

            DB::beginTransaction();
            Like::create([
                "user_id"=>$user->id,
                "image_id"=>$request->photoid,
                "video_id"=>null,
                "status"=>1
            ]);
            $to=array($image->user->device_token);
            // $message=$user->name.' Liked your image';
            $body["sender_name"]=$user->name;
            $body["sender_id"]=$user->id;
            $body["message"]='Like image';
            $body["photo"]=$user->photo;
            $body["liked_photo"]=$image->image;
            $this->send_fb_notification($to,$body,'like',$image->user->id);
            DB::commit();
            return response()->json(['status' => 1,'message'=>"You liked ".$image->user->name."'s image"]);
        }
        catch(\Exception $e){
            DB::rollback();
            return response()->json(['status' =>0,'error'=>$e]);
        }
    }
    public function like_video(Request $request){
        $validator = Validator::make($request->all(),[
          'videoid' => 'required',
          'status'=>'required'
        ]);
        if($validator->fails()){
            return response()->json(['status' => 0,'message'=>$this->get_errors($validator->messages())]);
        }
        $video=Video::find($request->videoid);
        $user=auth()->guard('api')->user();
        $alreadyLiked=Like::where('user_id',$user->id)->where('video_id',$request->videoid)->count();
        if($alreadyLiked){
            return response()->json(['status' => 0,'message'=>"You already liked ".$video->user->name."'s this image"]);
        }
        try{
            DB::beginTransaction();
            Like::create([
                "user_id"=>$user->id,
                "image_id"=>null,
                "video_id"=>$request->videoid,
                "status"=>1
            ]);
            DB::commit();
            return response()->json(['status' => 1,'message'=>"You liked ".$video->user->name."'s this video"]);
        }
        catch(\Exception $e){
            DB::rollback();
            return response()->json(['status' =>0,'error'=>$e]);
        }
    }
    public function follow_user(Request $request){
        $validator = Validator::make($request->all(), [
          'id' => 'required',
        ]);
        if($validator->fails()) {
            return response()->json(['status' => 0,'message'=>$this->get_errors($validator->messages())]);
        }
        $ifFollowingExists=User::find($request->id);
        $follower=auth()->guard('api')->user();
        $ifAlreadyFollowed=Follow::where('user_id',$follower->id)->where('following_id',$request->id)->count();
        if($ifAlreadyFollowed){
            return response()->json(['status' => 0,'message'=>'You already followed '.$ifFollowingExists->name]);
        }
        if($ifFollowingExists){
            if($request->id==$follower->id){
                return response()->json(['status' => 0,'message'=>'User can not follow himself']);
            }else{
                try{
                    DB::beginTransaction();
                    Follow::create([
                        "user_id"=>$follower->id,
                        "following_id"=>$request->id,
                        "status"=>1
                    ]);
                    $to=array($ifFollowingExists->device_token);
                    $message=$follower->name.' follows you';
                    $body["sender_name"]=$follower->name;
                    $body["sender_id"]=$follower->id;
                    $body["message"]='Followed you';
                    $body["photo"]=$follower->photo;
                    $this->send_fb_notification($to,$body,'follow',$ifFollowingExists->id);
                    DB::commit();
                    return response()->json(['status' =>1,'message'=>'You followed '.$ifFollowingExists->name]);
                }
                catch(\Exception $e){
                    DB::rollback();
                    return response()->json([$e]);
                }
            }
        }else{
            return response()->json(['status' => 0,'message'=>'User does not exist']);
        }
    }
    public function get_user_all_gallery(Request $request){
        $follower=auth()->guard('api')->user();
        $allImages=Photo::where('user_id',$follower->id)->orderByDesc('id')->paginate(5,['id','image','status'])->toArray();
        return response()->json([
            'status' =>1,
            'gallery'=>$allImages['data'],
            'first_page_url'=>$allImages['first_page_url'],
            'next_page_url'=>$allImages['next_page_url'],
            'path'=>$allImages['path'],
            'prev_page_url'=>$allImages['prev_page_url'],
            'total'=>$allImages['total']
        ]);
        // return response()->json(['status' =>1,'gallery'=>$allImages]);
    }
    public function get_user_public_gallery(Request $request){
        $follower=auth()->guard('api')->user();
        $allImages=Photo::where('user_id',$follower->id)->where('status',1)->paginate(5,['id','image','status']);
        $allImages=$allImages->toArray();
        unset($allImages["links"]);
        unset($allImages["from"]);
        unset($allImages["last_page"]);
        unset($allImages["last_page_url"]);
        unset($allImages["per_page"]);
        unset($allImages["to"]);
        unset($allImages["current_page"]);

        return response()->json([
            'status' =>1,
            'gallery'=>$allImages['data'],
            'first_page_url'=>$allImages['first_page_url'],
            'next_page_url'=>$allImages['next_page_url'],
            'path'=>$allImages['path'],
            'prev_page_url'=>$allImages['prev_page_url'],
            'total'=>$allImages['total']
        ]);
    }
    public function get_user_private_gallery(Request $request){
        $follower=auth()->guard('api')->user();
        $allImages=Photo::where('user_id',$follower->id)->where('status',0)->get(['id','image','status']);
        return response()->json(['status' =>1,'gallery'=>$allImages]);
    }
    public function get_user_video_gallery(Request $request){
        $user=auth()->guard('api')->user();
        $videos=Video::where('user_id',$user->id)->paginate(5,['id','video','status']);
        $videos=$videos->toArray();
        unset($videos["links"]);
        unset($videos["from"]);
        unset($videos["last_page"]);
        unset($videos["last_page_url"]);
        unset($videos["per_page"]);
        unset($videos["to"]);
        unset($videos["current_page"]);
        return response()->json([
            'status' =>1,
            'gallery'=>$videos['data'],
            'first_page_url'=>$videos['first_page_url'],
            'next_page_url'=>$videos['next_page_url'],
            'path'=>$videos['path'],
            'prev_page_url'=>$videos['prev_page_url'],
            'total'=>$videos['total']
        ]);
        
    }

    public function get_user_meta($user){
        if(gettype($user)!='array'){
            $user=$user->toArray();
        }
        $meta['followers']=Follow::where('following_id',$user['id'])->count();
        $meta['images']=Photo::where('user_id',$user['id'])->count();
        $meta['videos']=Video::where('user_id',$user['id'])->count();
        $meta['completed']=$this->profile_completion_percentage($user);
        return $meta;
    }
    public function notifications_list(Request $request){
        $user=auth()->guard('api')->user();
        $notifications=Notification::where('user_id',$user->id)->paginate(5,['id','body','created_at'])->toArray();
        return response()->json([
            'status' =>1,
            'total'=>count($notifications['data']),
            'notifications'=>$notifications['data'],
            'next_page_url'=>$notifications['next_page_url']
        ]);
      
    }
    public function like_notifications(){

        $user=auth()->guard('api')->user();
      
        $notifications=Notification::where(['user_id'=>$user->id,'action'=>'like'])->paginate(5,['id','body','created_at'])->toArray();
        return response()->json([
            'status' =>1,
            'total'=>count($notifications['data']),
            'notifications'=>$notifications['data'],
            'next_page_url'=>$notifications['next_page_url']
        ]);
    }
    public function follow_notifications(){
        $user=auth()->guard('api')->user();
        $notifications=Notification::where('user_id',$user->id)->where('action','follow')->paginate(5,['id','body','created_at'])->toArray();
        return response()->json([
            'status' =>1,
            'total'=>count($notifications['data']),
            'notifications'=>$notifications['data'],
            'next_page_url'=>$notifications['next_page_url']
        ]);
    }
    public function notification_ready(Request $request){
        $validator = Validator::make($request->all(),[
          'notificationid' => 'required',
        ]);
        if($validator->fails()){
            return response()->json(['status' => 0,'message'=>$this->get_errors($validator->messages())]);
        }
        $user=auth()->guard('api')->user();
        
        $notification=Notification::where('user_id',$user->id)->where('id',$request->notificationid)->first();
        $notification->update([
            "status"=>0,
        ]);

    }

   
    public function send_fb_notification($to,$sender,$action,$user_id){
        Notification::create([
            "user_id"=>$user_id,
            "who_send_notification"=>$sender['sender_id'],
            "body"=>json_encode($sender),
            "action"=>$action,
            "status"=>1
        ]);
       $data = [
            "registration_ids" => $to,
            "title" => $sender['sender_name'],
            "body" => $sender['message'],
            "image"=> $sender['photo'],
            "notification" => [
                "title" => $sender['sender_name'],
                "body" => $sender['message'],
                "image"=> $sender['photo'],
                
            ]
        ];
        $SERVER_API_KEY="AAAApgwMLnQ:APA91bGSl1LS2-mLQnLFjx1vIt_iY1BUDO4b2YzNkJN0kFagQtoJqz6SSB91NadsbNyPUQjo8uwGlvi2t6LY5uVgLV28dDqHV0wxz9PCkF4lG5_UkHEG3V5EKfR9Z5XOXU6sZXOe1udi";
        $dataString = json_encode($data);
        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }



    // restricted to this controller
    public function profile_completion_percentage($user){
        if(gettype($user)!='array'){
            $user= $user->toArray();    
        }
        unset($user["isOnline"]);
        unset($user["created_at"]);
        unset($user["updated_at"]);
        unset($user["email_verified_at"]);
        unset($user["lat"]);
        unset($user["lon"]);
        unset($user["postal_code"]);
        unset($user["state_name"]);
        unset($user["verify_token"]);
        unset($user["is_social"]);
        unset($user["status"]);
        unset($user["id"]);
        // unset($user["name"]);
        $percentage=0;
        $per_field=100/count($user);
        foreach ($user as $key => $value){
            if($value!=null){
                $percentage +=$per_field;
            }
        }
        if((int)$percentage==99){
            return 100;
        }
        return (int)$percentage;
    }
    public function get_errors($errors){
        foreach ($errors->get('*') as $key => $value){
            return $value[0];
        }
    }
}
