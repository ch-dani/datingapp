<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use DB;
use App\Models\User;
use App\Models\Follow;
use App\Models\Photo;
use App\Models\Video;
use App\Models\Searching;
use Illuminate\Database\Eloquent\Builder;
use App\Mail\forgot_pwd;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use GeoIP;
use Image;

class SearchController extends Controller
{
	protected $attrToUser=['id','name','email','phone','country','city','gender','relationship_status','dob','education','body_type','have_children','zodiac_sign','sexual_orientation','smoker','drink','sports','height','hair_color','eye_color','ethnicity','is_social','photo'];
    public function save_parameter(Request $request){
	  	//  $validator = Validator::make($request->all(), [
		 //      'education' => 'required',  //email may be email or phone number
		 //      'body_type' => 'required',
		 //      'have_children' => 'required',
		 //      'zodiac_sign' => 'required',
		 //      'sexual_orientation' => 'required',
		 //      'smoker' => 'required',
		 //      'drink' => 'required',
		 //      'sports' => 'required'
		     
		 //    ]);
			// if ($validator->fails()) {
			// 	return response()->json(['status' => 0,'message'=>$this->get_errors($validator->messages())]);
		 //    }
    	$user=auth()->guard('api')->user();
    	$data=$request->all();
    	$data['user_id']=$user->id;
    	try{
    		DB::beginTransaction();
    		$searchingExists=Searching::where('user_id',$user->id)->count();
    		if($searchingExists){
    			$search=Searching::where('user_id',$user->id)->first();
    			$search->update($data);
    		}else{
    			Searching::create($data);
    		}
    		DB::commit();
    		return response()->json(['status' =>1,'message'=>'User Searching Parameter set successfully']);
    	}
    	catch(\Exception $e){
    		DB::rollback();
    		return response()->json(['status' => 0,'message'=>'Database Exception, You cant hit this api again. if you want to update parameter please hit update parameter api']);
    	}
    }
    public function get_searching_parameter(Request $request){
    	$user=auth()->guard('api')->user();
    	$searching=Searching::where('user_id',$user->id)->first();
    	$user=$user->only($this->attrToUser);
    	return response()->json([
    		'status' =>1,
    		'user'=>$user,
    		'searching_param'=>$searching
    	]);
    	

    }
    public function update_search_parameter(Request $request){
    	$user=auth()->guard('api')->user();
    	$searching=Searching::where('user_id',$user->id)->first();
    	$data=$request->all();
    	
    	try{
    		DB::beginTransaction();
    		$searching->update($data);
    		DB::commit();
    		return response()->json(['status' =>1,'message'=>'User Searching Parameter updated successfully']);
    	}
    	catch(\Exception $e){
    		DB::rollback();
    		return response()->json(['status' => 0,'message'=>'Database Exception'.$e]);
    	}
    }
    public function get_user_with_advance_search(Request $request){
    	$user=auth()->guard('api')->user();
    	$sparams=Searching::where('user_id',$user->id)->first();
    	// return $sparams;
    	// return User::where(['status'=>6,'gnder'=>5])->get();
    	// return User::where('status',1)->OrWhere(['gender'=>5,'education'=>4])->get();
    	$users=User::where('id', '!=',$user->id )
        // ->where('gender',$user->lk_for)
        ->where('status',1)
        // ->where('country',$user->s_country)
        ->where(function ($query) use ($user,$sparams) {
            // $query->where('id', '!=',$user->id )
         //    ->where('status',1)
            $query->where("education",$sparams->education)
            ->orWhere("body_type",$sparams->body_type)
            ->orWhere("have_children",$sparams->have_children)
            ->orWhere("zodiac_sign",$sparams->zodiac_sign)
            ->orWhere('sexual_orientation',$sparams->sexual_orientation)
            ->orWhere('smoker',$sparams->smoker)
            ->orWhere('drink',$sparams->drink)
	        ->orWhere('relationship_status',$sparams->relationship_status)
	        ->orWhere('hair_color',$sparams->hair_color)
	        ->orWhere('eye_color',$sparams->eye_color)
	        ->orWhere('ethnicity',$sparams->ethnicity)
	        ;
	        // ->orWhere('responsiveness',$user->relationship_status)
	        // ->orWhere('attractiveness',$user->relationship_status)
	        // ->orWhere('travel',$user->relationship_status)
	        // ->orWhere('travel',$user->relationship_status)
	        // ->orWhere('travel',$user->relationship_status);
            
        })->paginate(5,$this->attrToUser);
    	$users=$users->toArray();
    	unset($users["links"]);
		unset($users["from"]);
		unset($users["last_page"]);
		unset($users["last_page_url"]);
		unset($users["per_page"]);
		unset($users["to"]);
		$users['status']=1;
		return response()->json([
    		'status' =>1,
    		'users'=>$users['data'],
    		'next_page_url'=>$users['next_page_url']
    	]);
    	
    	return $users;
    }
	// private to this controller
    public function get_errors($errors){
		foreach ($errors->get('*') as $key => $value){
    		return $value[0];
    	}
	}

}
